# VLLM

VLLM适配昇腾开源验证

VLLM是一个开源的高性能大语言模型推理库，旨在实现大规模语言模型的快速推理，本项目致力于实现VLLM适配华为昇腾处理器、鲲鹏处理器
和EulerOS操作系统上的推理部署演示。

**一  鲲鹏、OpenEuler验证**

第一步：在github上拉取相关项目。

git clone https://github.com/vllm-project/vllm.git


第二步，安装vllm，相关依赖项和配置，对自动安装，如
果提示缺少某个包，pip install xxx即可。

创建虚拟环境 
conda create -n vllm311 python=3.11

conda activate vllm311

pip install wheel packaging ninja setuptools>=49.4.0 numpy psutil

注:

      1.numpy版本建议安装1.23左右的，不要安装2.0及以上的，会有兼容问题。

      2.需要安装依赖环境 yum install -y gcc gcc-c++

      3.安装vllm 需要指定设备为CPU

           VLLM_TARGET_DEVICE=cpu

进入源码目录

pip install -v -r requirements-cpu.txt –extra-index-url https://download.pytorch.org/whl/cpu

执行安装

VLLM_TARGET_DEVICE=cpu python setup.py install

 vLLM默认从HuggingFace下载模型，如果想从ModelScope下载模型，需要配置环境变量。

export VLLM_USE_MODELSCOPE=True


第三步，运行脚本或者在终端命令行运行。

    运行脚本 python test_vllm_cpu.py
    
    验证结果:
    
 ![cpu.jpg](https://raw.gitcode.com/pan123/vllm/attachment/uploads/79a59f21-8465-4622-b9ba-2b354abd38dc/cpu.jpg 'cpu.jpg')
    
       
**二 NPU验证推理**

第一步：安装依赖包

NPU上需要CANN、torch_npu、pytorch

先安装CANN，参考：

https://www.hiascend.com/zh/developer/download/community/result?module=cann

torch_npu的安装参考：

https://github.com/Ascend/pytorch/blob/master/README.zh.md


第二步：安装vllm

conda create -n vllm310 python=3.10

conda activate vllm310

cd vllm 

VLLM_TARGET_DEVICE=npu pip install -e .

注：

    1.torch_npu、torch均下载2.4.0版本或者2.1.0，官方torch版本是2.5.1。
         
         pip install torch torch_npu

    2.安装cann包，确保版本匹配torch，本次验证CANN版本是 8.0 rc2。

    3.clone仓库，不要拉取最新的代码，否则安装会失败。下载支持npu安装的源码。

         git clone -b npu_support https://github.com/wangshuai09/vllm.git
      
    4.安装vllm 需要加上参数 VLLM_TARGET_DEVICE=npu

         VLLM_TARGET_DEVICE=npu pip install -e .
         
    5.numpy 版本1.23左右，本次验证安装版本 1.23.5

      
  
第三步，运行脚本或者在终端命令行运行

    cd vllm
    python examples/offline_inference_npu.py --trust-remote-code
   
注：

    1.需要加上参数 –trust-remote-code

    2.默认是huggingface，可以指定从modelscope下载

    3.安装 pip install modelscope

    4.VLLM github官网，可以查看支持的推理模型，本次验证选的的模型是:

        Qwen/Qwen1.5-4B-Chat
    
    验证结果:
    
    选择的是Qwen聊天对话模型，输入问题提交给模型后，模型反馈了答案。
    
    
   ![res.jpg](https://raw.gitcode.com/pan123/vllm/attachment/uploads/c90a9b95-3d78-4952-a9dc-34d8fcf0ae23/res.jpg 'res.jpg')
   
***本次验证资源清单

CPU架构:  鲲鹏计算 Kunpeng-920
NPU架构:  Ascend 910B3
操作系统:  Huawei Cloud EulerOS 2.0 (aarch64)
    
    